import { Component, OnInit } from '@angular/core';
//Se importa sevicio Auth 
import {AuthService} from '../../servicios/auth.service';
//Para hacer redireccion
import {Router} from '@angular/router';
import {FlashMessagesService} from 'angular2-flash-messages';


@Component({
  selector: 'app-pag-registro',
  templateUrl: './pag-registro.component.html',
  styleUrls: ['./pag-registro.component.scss']
})
export class PagRegistroComponent implements OnInit {

public email:string;
public password:string;

  constructor(
    public AuthService : AuthService,
    public router : Router, //Para redireccionar
    public flashMensaje : FlashMessagesService

  ) { }

  ngOnInit() {
  }
  onSubmitAddUser(){

   this.AuthService.registerUser(this.email,this.password)
   .then ((res)=>{
     this.flashMensaje.show('Usuario creado correctamente.',
     {cssClass: 'alert-success',timeout:4000});
     this.router.navigate(['/privado']);
     //Manejo de error
   }).catch ((err)=> {
      console.log(err);
   });

  }

}
