import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PagRegistroComponent } from './pag-registro.component';

describe('PagRegistroComponent', () => {
  let component: PagRegistroComponent;
  let fixture: ComponentFixture<PagRegistroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PagRegistroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PagRegistroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
