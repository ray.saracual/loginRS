import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

/* Nuevos componentes @ray_saracual*/
import {HomePageComponent} from './componentes/home-page/home-page.component';
import {LoginPageComponent} from './componentes/login-page/login-page.component';
import { PagRegistroComponent} from './componentes/pag-registro/pag-registro.component';
import {PrivadoPageComponent} from './componentes/privado-page/privado-page.component';
import {NotFoundComponent} from './componentes/not-found/not-found.component';
import {AuthGuard} from './guards/auth.guard';

const routes: Routes = [
{path:'',component:HomePageComponent },
{path:'login',component:LoginPageComponent},
{path:'register',component:PagRegistroComponent},
{path:'privado',component: PrivadoPageComponent,canActivate:[AuthGuard]},
{path:'**',component: NotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
