import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import{FormsModule} from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { HomePageComponent } from './componentes/home-page/home-page.component';
import { NavBarComponent } from './componentes/nav-bar/nav-bar.component';
import { PagRegistroComponent } from './componentes/pag-registro/pag-registro.component';
import { LoginPageComponent } from './componentes/login-page/login-page.component';
import { PrivadoPageComponent } from './componentes/privado-page/privado-page.component';
import { NotFoundComponent } from './componentes/not-found/not-found.component';
import {environment} from '../environments/environment';
//Se importa servicio con metodos para conexion
import {AuthService } from './servicios/auth.service';
//Conexion a Firebase
import {AngularFireModule} from 'angularfire2';
import {AngularFireAuthModule} from 'angularfire2/auth';
//Proteje las URL
import {AuthGuard} from './guards/auth.guard';
//Dependencias para mensajes flash
import {FlashMessagesModule} from 'angular2-flash-messages';
import {FlashMessagesService} from 'angular2-flash-messages';

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    NavBarComponent,
    PagRegistroComponent,
    LoginPageComponent,
    PrivadoPageComponent,
    NotFoundComponent
  ],
  imports: [ //Todos los modulos se inyectan aca
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    AngularFireAuthModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),  //inicializo app
    FlashMessagesModule //Mensaje Flash

  ],
  //Todos los servicios se inyectan como providers
  providers: [AuthService, AuthGuard,FlashMessagesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
