import { Injectable } from '@angular/core';
import {AngularFireAuth} from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import 'rxjs/add/operator/map';

@Injectable()
export class AuthService {

  constructor(
    public afAuth: AngularFireAuth


  ) { }

 //LoginGoogle 
 loginGoogle(){
 return this.afAuth.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider()); 

 }
//Metodo para creacion de usuario

registerUser(email:string, pass:string){

  return new Promise ((resolve,reject) =>{

     this.afAuth.auth.createUserWithEmailAndPassword(email,pass)
     .then(userData=> resolve(userData),
     err => reject(err));
 });
}

//Metodo para login
 
loginEmail(email:string, pass:string){

  return new Promise ((resolve,reject) =>{
                      
     this.afAuth.auth.signInWithEmailAndPassword(email,pass)
     .then(userData=> resolve(userData),
     err => reject(err));
 });
}


//Metodo devolver datos de usuario logueado
 
getAuth(){
  return this.afAuth.authState.map (auth => auth);
}

//Metodo para cierre de sesion
  logout(){
    return this.afAuth.auth.signOut(); 
  }

}
