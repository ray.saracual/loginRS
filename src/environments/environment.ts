// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
    // Inicializo Firebase ambiente Desarrollo
  firebaseConfig:{
    apiKey: "AIzaSyAhwrR86cQp56XBbrbv0thqA1vLmbrvZmU",
    authDomain: "drivetest-344a6.firebaseapp.com",
    databaseURL: "https://drivetest-344a6.firebaseio.com",
    projectId: "drivetest-344a6",
    storageBucket: "drivetest-344a6.appspot.com",
    messagingSenderId: "1080718715442"
    }
};
